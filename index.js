var MCrypt = require('mcrypt').MCrypt;
var Buffer = require('buffer').Buffer
var SECRET_KEY_PHP         = "74727565";//true
var SECRET_KEY_JAVA = "747275656964747275656964";//trueidtrueid
var IV                = new Buffer("\0\0\0\0\0\0\0\0");
var ENCODING = 'hex'

function chr(n) {
    if (n < 128) {
        return String.fromCharCode(n);
    }
}
function pkcs5_pad(text, blocksize)
{
	var pad = blocksize - (text.length % blocksize);

	for(i=0;i<pad;i++){
		text = text+chr(pad);
	}
	return text;
}

function encryptPHP(input)
{
	var desEcb = new MCrypt('des', 'ecb');
	desEcb.open(SECRET_KEY_PHP); // we are set the key

	var ciphertext = desEcb.encrypt(input);
	ciphertext = new Buffer(ciphertext).toString('base64');
	
	return ciphertext;
}

function decryptPHP(input)
{
	var desEcb = new MCrypt('des', 'ecb');
	desEcb.open(SECRET_KEY_PHP); // we are set the key
	
	var binary = new Buffer(input, 'base64');

	var plaintext = desEcb.decrypt(binary);
	plaintext = plaintext.toString().replace(/[\x00-\x1F\x80-\xFF]/g,"");

	return plaintext;
}

function encryptJAVA(input)
{
	var desEcb = new MCrypt('tripledes', 'ecb');
	console.log(desEcb.getSupportedKeySizes());
	desEcb.open(SECRET_KEY_JAVA); // we are set the key
	
	input = new Buffer(input).toString('base64');
	input = pkcs5_pad(input,desEcb.getBlockSize());
	console.log('input = '+input);

	//Buffers can be used for taking a string or piece of data and doing base64 encoding of the result. For example:
	var ciphertext = desEcb.encrypt(input);
	ciphertext = new Buffer(ciphertext).toString('base64');
	
	return ciphertext;
}

function decryptJAVA(input)
{
	var desEcb = new MCrypt('tripledes', 'ecb');
	//console.log(desEcb.getSupportedKeySizes());
	desEcb.open(SECRET_KEY_JAVA); // we are set the key
	
	var binary = new Buffer(input, 'base64');
	//console.log('binary = '+binary);
	var plaintext = desEcb.decrypt(binary);
	plaintext = plaintext.toString().replace(/[\x00-\x1F\x80-\xFF]/g,"");
	//console.log('plaintext = '+plaintext);
	plaintext = new Buffer(plaintext, 'base64');   
	//console.log('plaintext = '+plaintext);
	
	return plaintext;
}

module.exports = {
  encryptPHP: function(input) {
    return encryptPHP(input);
  },

  /**
   * Unescape special characters in the given string of html.
   *
   * @param  {String} html
   * @return {String}
   */
  decryptPHP: function(input) {
    return decryptPHP(input);
  },
  encryptJAVA: function(input) {
    return encryptJAVA(input);
  },
  decryptJAVA: function(input) {
    return decryptJAVA(input);
  }
};